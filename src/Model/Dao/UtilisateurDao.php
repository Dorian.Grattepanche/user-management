<?php
namespace DWWM\Model\Dao;

use \PDO;
use DWWM\Model\Dal\Dal;

class UtilisateurDao extends Dal
{
    private $classname = "DWWM\\Model\\Classes\\Utilisateur";
    private $construct_args = ['id', 'login', 'password'];

    public function getUtilisateur($login, $password)
    {
        // Requête SQL
        $query = "SELECT * FROM `utilisateur`
                  WHERE `login` = :login
                  AND   `password` = :password;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":login", $login);
        $sth->bindParam(":password", $password);
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération du résultat
        $item = $sth->fetch();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $item;
    }

    public function get($id)
    {
        // Requête SQL
        $query = "SELECT * FROM `utilisateur`
                  WHERE `id` = :id;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id", $id);
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération du résultat
        $item = $sth->fetch();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $item;
    }

    public function getAll()
    {
        // Requête SQL
        $query = "SELECT * FROM `utilisateur`;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        // pas de paramètres ...

        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération du résultat
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }
}