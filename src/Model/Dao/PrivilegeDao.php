<?php
namespace DWWM\Model\Dao;

use \PDO;
use DWWM\Model\Dal\Dal;

class PrivilegeDao extends Dal
{
    private $classname = "DWWM\\Model\\Classes\\Privilege";
    private $construct_args = ['id', 'nom'];

    public function getPrivilegesByUtilisateur($id_utilisateur)
    {
        // Requête SQL
        $query = "SELECT `privilege`.* FROM `privilege`
                  INNER JOIN `groupe_privilege`
                    ON `id_privilege` = `privilege`.`id`
                  INNER JOIN `utilisateur_groupe`
                    ON `groupe_privilege`.`id_groupe` = `utilisateur_groupe`.`id_groupe`
                  WHERE `id_utilisateur` = :id_utilisateur";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id_utilisateur", $id_utilisateur);
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }

    public function getAll()
    {
        // Requête SQL
        $query = "SELECT * FROM `privilege`;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }
}