<?php
namespace DWWM\Model\Dao;

use \PDO;
use DWWM\Model\Dal\Dal;

class GroupeDao extends Dal
{
    private $classname = "DWWM\\Model\\Classes\\Groupe";
    private $construct_args = ['id', 'nom'];

    public function getAll()
    {
        // Requête SQL
        $query = "SELECT * FROM `groupe`;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }

    public function getGroupesByUtilisateur($id_utilisateur)
    {
        // Requête SQL
        $query = "SELECT `groupe`.* FROM `groupe`
                  INNER JOIN `utilisateur_groupe`
                        ON `id_groupe` = `groupe`.`id`
                  WHERE `id_utilisateur` = :id_utilisateur";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id_utilisateur", $id_utilisateur);
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }

    public function getNotAffectedGroupesByUtilisateur($id_utilisateur)
    {
        // Requête SQL
        $query = "SELECT * FROM `groupe`
                  WHERE id NOT IN
                  (
                      SELECT DISTINCT `id_groupe`
                      FROM `utilisateur_groupe`
                      WHERE `id_utilisateur` = :id_utilisateur
                  );";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        $sth->bindParam(":id_utilisateur", $id_utilisateur);
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }
}