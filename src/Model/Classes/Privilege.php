<?php
namespace DWWM\Model\Classes;

use DWWM\Model\Dao\PrivilegeDao;

class Privilege
{
    // Propriété(s)
    public $id;
    public $nom;

    // Constructeur
    public function __construct($id, $nom)
    {
        $this->id = $id;
        $this->nom = $nom;
    }

    // Méthodes statiques

    public static function getPrivilegesByUtilisateur($id_utilisateur)
    {
        $dao = new PrivilegeDao();
        return $dao->getPrivilegesByUtilisateur($id_utilisateur);
    }
    public static function getAll()
    {
        $dao = new PrivilegeDao();
        return $dao->getAll();
    }
}