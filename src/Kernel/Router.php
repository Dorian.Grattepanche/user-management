<?php
namespace DWWM\Kernel;

class Router
{
    private static $_routes = [];

    public static function getPath()
    {
        $script_name = $_SERVER['SCRIPT_NAME'];
        $exploded = explode('/', $script_name);
        array_pop($exploded);
        $path = implode('/', $exploded);
        return $path;
    }

    public static function getOrigin()
    {
        return $_SERVER['SERVER_NAME'];
    }

    public static function getParameters()
    {
        $uri_left = explode('?',$_SERVER['REQUEST_URI'])[0];
        $path = self::getPath();
        $length = strlen($path);
        $parameters = substr($uri_left, $length);

        return $parameters;
    }

    public static function addRoute($route)
    {
        array_push(self::$_routes, $route);
    }

    public static function findRoute()
    {
        $route = null;
        $match = false;
        $count = count(self::$_routes);
        $parameters = self::getParameters();

        for($index = 0; $index < $count && ! $match; $index++)
        {
            $current = self::$_routes[$index];
            $match = $current->match($parameters);
        }

        if($match)
        {
            $route = $current;
        }

        return $route;
    }
}