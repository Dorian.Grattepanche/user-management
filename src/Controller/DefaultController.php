<?php
namespace DWWM\Controller;

use DWWM\Kernel\Router;
use DWWM\Kernel\SessionManager;
use DWWM\View\View;

class DefaultController
{
    public static function welcomeAction()
    {
        $origin = Router::getOrigin();
        $path = Router::getPath();

        $user = SessionManager::getUser();
        $groupes = SessionManager::getGroupes();
        $privileges = SessionManager::getPrivileges();
        
        $isConnected = SessionManager::isConnected();

        $view = new View("welcome");
        $view->bindParam("origin", $origin);
        $view->bindParam("path", $path);
        $view->bindParam("user", $user);
        $view->bindParam("groupes", $groupes);
        $view->bindParam("privileges", $privileges);
        $view->bindParam("isConnected", $isConnected);
        $view->display();
    }

    public static function _404Action()
    {
        $origin = Router::getOrigin();
        $path = Router::getPath();

        $user = SessionManager::getUser();
        $groupes = SessionManager::getGroupes();
        $privileges = SessionManager::getPrivileges();
        $isConnected = SessionManager::isConnected();

        $view = new View("404");
        $view->bindParam("origin", $origin);
        $view->bindParam("path", $path);
        $view->bindParam("user", $user);
        $view->bindParam("groupes", $groupes);
        $view->bindParam("privileges", $privileges);
        $view->bindParam("isConnected", $isConnected);
        $view->display();
    }
}