<?php
namespace DWWM\Controller;

use DWWM\Kernel\Router;
use DWWM\Kernel\SessionManager;

use DWWM\Model\Classes\Affectation;

class AffectationController
{
    public static function affectAction()
    {
        $id_utilisateur = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
        $id_groupe = filter_input(INPUT_POST, "not_affected_groupes", FILTER_SANITIZE_NUMBER_INT);

        if(! empty($id_groupe))
        {
            Affectation::affect($id_utilisateur, $id_groupe);
        }

        // retour sur la page User Update
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "/User/{$id_utilisateur}";
        header("location: http://{$origin}{$path}{$options}");
    }

    public static function disaffectAction()
    {
        $id_utilisateur = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
        $id_groupe = filter_input(INPUT_POST, "affected_groupes", FILTER_SANITIZE_NUMBER_INT);

        if(! empty($id_groupe))
        {
            Affectation::disaffect($id_utilisateur, $id_groupe);
        }

        // retour sur la page User Update
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "/User/{$id_utilisateur}";
        header("location: http://{$origin}{$path}{$options}");
    }
}